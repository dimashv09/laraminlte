# Laradmin LTE

## Requirement
[Laravel 7](https://laravel.com/) + [Admin LTE 3](https://adminlte.io/)

## Installation
1. Clone this repo
2. Run `composer install`. If you don't have composer. Install [from here](https://getcomposer.org/)
3. Copy .env.example to file named .env or just run command `cp .env.example .env` to do that
4. Set the .env configuration properly
5. Run `php artisan key:generate`
6. If you need migrate the DB, run `php artisan migrate` or `php artisan migrate --seed` to run the seeder too
7. Run the server with command `php artisan serve`